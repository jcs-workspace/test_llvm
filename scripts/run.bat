@echo off
:: ========================================================================
:: $File: run.bat $
:: $Date: 2023-04-01 16:20:33 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

start ./bin/test_llvm.exe
