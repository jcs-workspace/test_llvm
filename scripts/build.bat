@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-04-01 16:19:25 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

clang main.cpp -o ./bin/test_llvm.exe
